<?php

use Books\DB;
use Books\Middleware\JsonBodyParserMiddleware;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\Factory\AppFactory;

require __DIR__ . '/../vendor/autoload.php';

//ini_set('display_errors', 1); ini_set('display_startup_errors', 1); error_reporting(E_ALL);
//$book=new \Books\Books();



$database = new \Dibi\Connection([
    'driver'   =>'sqlite',
    'database' => __DIR__ . '/../src/hw-09.db'
]);

const USERNAME = 'admin';
const PASSWORD = 'pas$word';

$app = AppFactory::create();

$app->addRoutingMiddleware();
$app->addErrorMiddleware(true, true, true);
$app->add(new JsonBodyParserMiddleware());


function checkAuthorization():bool {
    if(!isset($_SERVER['PHP_AUTH_PW']) || !isset($_SERVER['PHP_AUTH_USER']))
        return false;

    if ($_SERVER['PHP_AUTH_PW'] != PASSWORD || $_SERVER['PHP_AUTH_USER'] != USERNAME)
        return FALSE;
    return TRUE;
}


$app->get('/books', function (Request $request, Response $response, $args) use($database) {
    $payload = json_encode(
        $database->select('id,name,author')->from('books')->fetchAll()
    );
    $response->getBody()->write($payload);
    return $response->withHeader('Content-Type', 'application/json')->withStatus(200);
});


$app->get('/books/{id}', function (Request $request, Response $response, $args) use($database) {
    $route = $request->getAttribute('route');
    $courseId = $route->getArgument('id');
    $result=$database->select('*')->from('books')->where('id=%i',$courseId)->fetchAll();
    if(sizeof($result)==0)
        return $response->withStatus(404);
    $payload = json_encode($result);

    $response->getBody()->write($payload);
    return $response->withHeader('Content-Type', 'application/json')->withStatus(200);
});




$app->post('/books', function (Request $request, Response $response, $args) use($database) {

    if (FALSE === checkAuthorization()) {
        return $response->withStatus(401);
    }

    $data = $request->getParsedBody();
    if(!isset($data['name']) || !isset($data['author']) || !isset($data['publisher']) || !isset($data['isbn']) || !isset($data['pages']))
        return $response->withStatus(400);

    $result = $database->insert('books', [
        'name' => $data['name'],
        'author' => $data['author'],
        'publisher' => $data['publisher'],
        'isbn' => $data['isbn'],
        'pages' => $data['pages']
    ])->execute() !== FALSE;
 $id=$database->getInsertId();
   header("Location: /books/".$id );
   return $response->withStatus(201);
});







$app->put('/books/{id}', function (Request $request, Response $response, $args) use($database) {
    if (FALSE === checkAuthorization()) {
        return $response->withStatus(401);
    }
    $data = $request->getParsedBody();
    $route = $request->getAttribute('route');
    $courseId = $route->getArgument('id');
    $result=$database->select('*')->from('books')->where('id=%i',$courseId)->fetchAll();
    if((sizeof($result)==0))
        return $response->withStatus(404);

    if(!isset($data['name']) || !isset($data['author']) || !isset($data['publisher']) || !isset($data['isbn']) || !isset($data['pages']))
        return $response->withStatus(400);

    $database->query('UPDATE books SET', [
        'name' => $data['name'],
        'author' => $data['author'],
        'publisher' => $data['publisher'],
        'isbn' => $data['isbn'],
        'pages' => $data['pages']
    ], 'WHERE id = ?', $courseId);
    return $response->withStatus(204);
});



$app->delete('/books/{id}', function (Request $request, Response $response, $args) use($database) {
    if (FALSE === checkAuthorization()) {
        return $response->withStatus(401);
    }
    $route = $request->getAttribute('route');
    $courseId = $route->getArgument('id');
    $result=$database->select('*')->from('books')->where('id=%i',$courseId)->fetchAll();
    if((sizeof($result)==0))
        return $response->withStatus(404);
    $database->delete('books')->where('id=%i',$courseId)->execute();
    return $response->withStatus(204);
});


$app->run();


