<?php


namespace Books;

require __DIR__ . '/../vendor/autoload.php';

class Books
{

    /**
     * Books constructor.
     */
    public function __construct()
    {   $db = Db::get();
        $db->query('CREATE TABLE IF NOT EXISTS `books` (
            `id` INTEGER PRIMARY KEY AUTOINCREMENT,
            `name` TEXT NOT NULL,
            `author` TEXT NOT NULL,
            `publisher` TEXT NOT NULL,
            `isbn` TEXT NOT NULL,
            `pages` INTEGER NOT NULL 
        )');
    }

}
new Books();





