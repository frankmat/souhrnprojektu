<?php


namespace Books;


class DB
{
    protected static $pdo = null;

    public static function get(): \PDO
    {
        return self::$pdo ?? (self::$pdo = new \PDO(
                 'sqlite:hw-09.db',
                null,
                null,
                [
                    \PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION
                ]
            ));
    }
}