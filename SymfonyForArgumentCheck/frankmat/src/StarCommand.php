<?php

namespace Star;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class StarCommand extends Command
{
    protected static $defaultName = 'star';

    protected function configure()
    {
        $this
            ->setName('draw:polygon')
            ->setDescription("Parametry pro hvezdu")
            ->addArgument('width', InputArgument::REQUIRED, 'výška a šířka výsledného obrázku')
            ->addArgument('color', InputArgument::REQUIRED, 'barva hvězdy')
            ->addArgument('points', InputArgument::REQUIRED, 'počet cípů hvězdy')
            ->addArgument('radius', InputArgument::REQUIRED, 'hodnota od 0 do 1, která definuje "vykousnutí" cípů')
            ->addArgument('output', InputArgument::REQUIRED, 'název souboru, do kterého se obrázek uloží')
            ->addArgument('bgColor', InputArgument::OPTIONAL, 'barva pozadí, výchozí hodnota je bílá')
            ->addArgument('borderColor', InputArgument::OPTIONAL, 'barva rámečku, pokud není zadaná, rámeček se nevykreslí')
            ->addArgument('borderWidth', InputArgument::OPTIONAL, 'šířka rámečku v px, pokud není zadaná, rámeček se nevykreslí');


    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $width = $input->getArgument('width');
        $color = $input->getArgument('color');
        $points = $input->getArgument('points');
        $radius = $input->getArgument('radius');
        $output = $input->getArgument('output');
        $bgColor = $input->getArgument('bgColor');
        $borderColor = $input->getArgument('borderColor');
        $borderWidth = $input->getArgument('borderWidth');

        $image = imagecreatetruecolor($width, $width);
        if (isset($bgColor))
            imagefill($image, 0, 0, $bgColor);
        else
            imagefill($image, 0, 0, imagecolorallocate($image, 255, 255, 255));


        $x0 = $width / 2;
        $y0 = $width / 2;
        $N = $points; // Number of spikes
        $astep = (2 * M_PI) / $N; // Slice the full circle by the number of spikes
        $points = array(); // Initialize the array of points
        $innerRadius = ($width / 2) * $radius;
        $outerRadius = $width / 2;
        for ($n = 0; $n < $N; $n++) {
            $thisangle = $astep * (float)$n; // Calculate the current total angle

            $x_n = $x0 + ($outerRadius * cos($thisangle - M_PI / 2)); // Calculate the n-th x deducing it from the angle, we also rotate a half PI to have one spike pointing exactly UP (by substracting M_PI/2 from the angle).
            array_push($points, $x_n); // Push the value in the points array

            $y_n = $y0 + ($outerRadius * sin($thisangle - M_PI / 2)); // Calculate the n-th y deducing it from the angle, we also rotate a half PI to have one spike pointing exactly UP (by substracting M_PI/2 from the angle).
            array_push($points, $y_n); // Push the value in the points array

            // Now the "inner" point x cohordinate
            $x_ni = $x0 + ($innerRadius * cos($thisangle - M_PI / 2 + M_PI / $N)); // we just add 1/4 PI to the preceding point and use the inner radius instead of R
            array_push($points, $x_ni); // Push the value in the points array

            // Now the "inner" point y cohordinate
            $y_ni = $y0 + ($innerRadius * sin($thisangle - M_PI / 2 + M_PI / $N)); // we just add 1/4 PI to the preceding point and use the inner radius instead of R
            array_push($points, $y_ni); // Push the value in the points array
        }

        imagefilledpolygon($image, $points, $N * 2, $color);

        if (substr($output, -4) != ".png")
            $output .= ".png";
        imagepng($image, $output);

        return 0;
    }

}
