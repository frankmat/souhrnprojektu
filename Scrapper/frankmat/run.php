<?php

use Goutte\Client;
use Symfony\Component\DomCrawler\Crawler;

require __DIR__.'/vendor/autoload.php';

function text(Crawler $crawler, string $selector)
{
    $new = $crawler->filter($selector);
    if (count($new)) {
        return trim($new->text());
    }

    return null;
}

/**
 * @param string $query - query string e.g. 'Beats Studio 3'
 * @return array
 */
function scrape(string $query)
{

    $client = new Client();
    $alza='https://www.alza.cz/search.htm?exps='.$query;
    $mironet='https://www.mironet.cz/Productlist/showSearch?EXPF='.$query;

    $client->setHeader('user-agent', "Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2272.101 Safari/537.36");

    $crawler_alza = $client->request('GET', $alza);
    $crawler_mironet=$client->request('GET',$mironet);


    $results= $crawler_alza->filter('.box.browsingitem.js-box.canBuy.inStockAvailability')->each(function($profile) {
        $counter=0;
        $result=[];;
        $name = $profile->filter('.name.browsinglink')->text();
        $priceDiv = $profile->filter('.c2')->text();
        $description=$profile->filter('.Description')->text();
        $description=trim($description);
        $link=$profile->filter('.name.browsinglink')->extract(array('href'));
        $fulllink="https://www.alza.cz" . $link[0];
        $result['name']=$name;
        $result['price']=$priceDiv;
        $result['link']=$fulllink;
        $result['eshop']="Alza";
        $result['description']=$description;
        return $result;
    });



    $results2= $crawler_mironet->filter('.item_b')->each(function($profile) {
        $counter=0;
        $result=[];
        $name = $profile->filter('.nazev')->text();
        $name=trim($name);
        $priceDiv = $profile->filter('.item_cena')->text();
        $priceDiv=trim($priceDiv);
        $description=$profile->filter('.popis')->text();
        $description=trim($description);
        $link=$profile->filter('.item_b_name')->extract(array('href'));
        $fulllink="https://www.mironet.cz/" . $link[0];
        $result['name']=$name;
        $result['price']=$priceDiv;
        $result['link']=$fulllink;
        $result['eshop']="Mironet";
        $result['description']=$description;
        return $result;
    });


    $result=array_merge($results,$results2);
    return $result;
}

/**
 * @param string $query   - query string e.g. 'Beats Studio 3'
 * @param array  $results - input product collection
 * @return array
 */
function cmp($a, $b) {
    return strcmp($a["price"], $b["price"]);
}

function filter(string $query, array $results)
{
    $exploded_query=explode(" ",$query);
    $results2=[];

    foreach ($results as $value)
    {
        $exploded=explode(" ",$value['name']);
        if($exploded[0]==$exploded_query[0])
            $results2[]=$value;
    }
    //var_dump($results2);


    //usort($results2, "cmp"); // neslo mi oddelat mezery, celkovce mi nefungovaly regexy, takze se omlouvam je to bez serazeni

    return $results2;
}

/**
 * input array $results show contain following keys
 * - 'name'
 * - 'price'
 * - 'link' - link to product detail page
 * - 'eshop' - eshop identifier e.g. 'alza'
 * - 'description'
 *
 * @param array $results
 */
function printResults(array $results, bool $includeDescription = false)
{


    $width = [
        'name' => 0,
        'price' => 0,
        'link' => 0,
        'eshop' => 0,
        'description' => 0,
    ];
    foreach ($results as $result) {
        foreach ($result as $key => $value) {
            $width[$key] = max(mb_strlen($value), $width[$key]);
        }
    }

    echo '+'.str_repeat('-', 2 + $width['name']);
    echo '+'.str_repeat('-', 2 + $width['price']);
    echo '+'.str_repeat('-', 2 + $width['link']);
    echo '+'.str_repeat('-', 2 + $width['eshop'])."+\n";
    foreach ($results as $result) {

        echo '| '.$result['name'].str_repeat(' ', $width['name'] - mb_strlen($result['name'])).' ';
        echo '| '.$result['price'].str_repeat(' ', $width['price'] - mb_strlen($result['price'])).' ';
        echo '| '.$result['link'].str_repeat(' ', $width['link'] - mb_strlen($result['link'])).' ';
        echo '| '.$result['eshop'].str_repeat(' ', $width['eshop'] - mb_strlen($result['eshop'])).' ';
        echo "|\n";
        echo '+'.str_repeat('-', 2 + $width['name']);
        echo '+'.str_repeat('-', 2 + $width['price']);
        echo '+'.str_repeat('-', 2 + $width['link']);
        echo '+'.str_repeat('-', 2 + $width['eshop'])."+\n";
        if ($includeDescription) {
            echo '| '.$result['description'].str_repeat(' ',
                    max(0, 7 + $width['name'] + $width['price'] + $width['link'] - mb_strlen($result['description'])));
            echo "|\n";
            echo str_repeat('-', 10 + $width['name'] + $width['price'] + $width['link'])."\n";
        }
    }

}

// MAIN
if (count($argv) !== 2) {
    echo "Usage: php run.php <query>\n";
    exit(1);
}

$query = $argv[1];
$results = scrape($query);
$results = filter($query, $results);
printResults($results);
